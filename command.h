#ifndef COMMAND_H
#define COMMAND_H

#include <functional>

template<class Receiver>
class Command
{
public:   
    using _action = std::function<bool(Receiver&, uint8_t*)>;

    Command(Receiver* rec, _action _act):_receiver(rec),action(_act){}
    virtual void execute(uint8_t* data) {
        //_receiver->action(ptr);
        action(*_receiver, data);
    }
private:
    //std::function<bool()> action;
    Receiver* _receiver;
    _action action;
};

#endif // COMMAND_H
