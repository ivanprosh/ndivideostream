import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
//import QtQml.StateMachine 1.0

import MainThread 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Timer {
        id: captureTimer
        //interval:
        repeat: true

    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page {           
            Item {
                id: mainContext
                property var currentState: MainThr.curState
                //property alias button1: button1
                anchors.centerIn: parent

                ColumnLayout {
                    width: 100
                    height: 0
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: 60
                    anchors.top: parent.top

                    Button {
                        id: button1
                        text: qsTr("Start")
                        //visible: CaptureWorkThr.curState == SeparateThread.StoppedState
                        onClicked: {
                            if(MainThr.curState == MainThreadNamespace.StoppedState)
                                MainThr.setCurState(MainThreadNamespace.CapturingState)
                            else
                                MainThr.setCurState(MainThreadNamespace.StoppedState)
                        }

                        Component.onCompleted: console.log(MainThreadNamespace.StoppedState)


                    }
                    /*
                    Row {
                        anchors.centerIn: parent.horizontalCenter
                        TextField {
                            id: framesCount
                            width: 50
                            placeholderText: "count"
                            //anchors.horizontalCenter: parent.horizontalCenter
                            text: CaptureWorkThr.frCount
                        }
                        TextField {
                            id: fps
                            width: 50
                            placeholderText: "count"
                            //anchors.horizontalCenter: parent.horizontalCenter
                            text: CaptureWorkThr.fps
                        }
                    }
                    */

                }

                states: [
                    State {
                        name: "captureStarted"; when: mainContext.currentState == MainThreadNamespace.CapturingState
                        PropertyChanges { target: button1; text: qsTr("Stop Capture") }
                        PropertyChanges { target: captureTimer; running: true }
                    },
                    State {
                        name: "captureStopped"; when: mainContext.currentState == MainThreadNamespace.StoppedState
                        PropertyChanges { target: button1; text: qsTr("Start Capture") }
                        PropertyChanges { target: captureTimer; running: false }
                    }
                ]
            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("First")
        }
        TabButton {
            text: qsTr("Second")
        }
    }
}
