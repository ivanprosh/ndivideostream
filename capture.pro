TEMPLATE = app
QT += core quick #qml

CONFIG += c++11
CONFIG(release, debug|release) {
    DEFINES += QT_NO_DEBUG_OUTPUT
}

SOURCES += main.cpp \
    capturecontrol.cpp \
    separatethread.cpp \
    mainthr.cpp \
    ndisender.cpp

RESOURCES += qml.qrc

#NDI Lib
#LIBS += -L../capture/lib \
#            -lProcessing.NDI.Lib.x86

LIBS += -L../capture/lib \
            -lProcessing.NDI.Lib.x86
DEPENDPATH += lib/
INCLUDEPATH += include/

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
#DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
#DEFINES += QML_BUILD_INQRC
contains(DEFINES, QML_BUILD_INQRC) {
    RESOURCES += $$files(*.qml)
}

content.files = $$files(*.qml) \
                lib/*

win32 {
    message("win build")

    target.path = $$OUT_PWD
    content.path = $$OUT_PWD

    !contains(DEFINES, QML_BUILD_INQRC) {
        INSTALLS += content
    }
    INSTALLS += target
}

#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target

HEADERS += \
    capturecontrol.h \
    separatethread.h \
    mainthr.h \
    ndisender.h \
    command.h
