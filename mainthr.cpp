#include "mainthr.h"
#include <QDebug>

void MainThr::setCurState(MainThr::State curState)
{
    if (m_curState == curState)
        return;

    qDebug() << "Curstate is " << (int)curState;

    m_curState = curState;
    emit curStateChanged(curState);
}
