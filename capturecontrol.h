#ifndef CAPTURECONTROL_H
#define CAPTURECONTROL_H

#include <QObject>
#include <atomic>
#include <functional>

class QTimer;
class NDISender;

template<class Receiver>
class Command;

class CaptureControl : public QObject
{
    Q_OBJECT

public:
    explicit CaptureControl(NDISender* _sender, QObject *parent = 0);
    ~CaptureControl();

    std::atomic<bool> running;

signals:
    void error( const QString& text );
//    void frCountChanged(quint32 frcount);
//    void fpsChanged(short fps);

public slots:
    //void setFrCount(quint32 frcount);
    void run();


private:
    friend class SeparateThread;
    quint32 count;
    Command<NDISender>* NDIsendCmd;
    NDISender* sender;
};

#endif // CAPTURECONTROL_H
