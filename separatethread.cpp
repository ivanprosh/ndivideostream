#include <QTimer>
#include "separatethread.h"
#include "ndisender.h"

const char* _connection_string = "<ndi_product long_name=\"NDILib Send Example.\" "
                                 "             short_name=\"NDILib Send\" "
                                 "             manufacturer=\"CoolCo, inc.\" "
                                 "             version=\"1.000.000\" "
                                 "             session=\"default\" "
                                 "             model_name=\"S1\" "
                                 "             serial=\"ABCDEFG\"/>";

SeparateThread::SeparateThread(QObject *parent):QThread(parent),countBefore(0),m_fps(0){
    _updateTimer = new QTimer(this);
    _updateTimer->setInterval(1000);
    connect(_updateTimer, SIGNAL(timeout()), this, SLOT(dataUpdate()));
}

SeparateThread::~SeparateThread()
{
    this->disconnect();
    delete m_worker;
}

void SeparateThread::run()
{
    qDebug() << "!SeparateThread::Init thread";
    // Create worker object within the context of the new thread
    //_sender = new NDIsender(_connection_string);
    m_worker = new CaptureControl(new NDISender(_connection_string),this);


    // forward to the worker: a 'queued connection'!
    connect( this, SIGNAL(captureStart()) ,
             m_worker, SLOT(run()));
    //forward a signal back out
    connect( m_worker, SIGNAL( error(QString)),
             this, SIGNAL( error( QString ) ) );
//    connect( m_worker, SIGNAL( frCountChanged(quint32)),
//             this, SIGNAL( frCountChanged(quint32)));
//    connect( m_worker, SIGNAL( fpsChanged(short)),
//             this, SIGNAL( fpsChanged(short)));

    exec();  // start our own event loop
}

void SeparateThread::curStateChanged(MainThr::State state)
{
    qDebug() << "From thr id " << QThread::currentThreadId() <<  ", state is " << static_cast<int>(state);

    if(state == MainThr::State::CapturingState) {
       _updateTimer->start();
       m_worker->running = true;
       emit captureStart();
    } else {
       _updateTimer->stop();
       m_worker->running = false;
    }
}
void SeparateThread::dataUpdate()
{
    //qDebug() << "Update info!";
    emit frCountChanged(m_worker->count);
    m_fps = (m_worker->count)-countBefore;
    emit fpsChanged(m_fps);
    countBefore = m_worker->count;
}
