#ifndef MAINTHR_H
#define MAINTHR_H

#include <QObject>

class MainThr : public QObject
{
    Q_OBJECT

    Q_PROPERTY(State curState READ curState WRITE setCurState NOTIFY curStateChanged)
    Q_ENUMS(State)

public:
    enum class State { StoppedState, CapturingState };

    MainThr() : m_curState(State::StoppedState) {}
    State curState() const {  return m_curState; }

public slots:
    void setCurState(State curState);

signals:
    void curStateChanged(MainThr::State curState);

private:
    State m_curState;
};

#endif // MAINTHR_H
