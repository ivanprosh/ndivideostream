#include <QGuiApplication>
#include <QtCore/QCoreApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>

#include <QWindow>
#include <QDebug>
#include <QDir>
#include <QScreen>
#include <QBuffer>
#include <QPixmap>

#include "separatethread.h"
//#include "ndisender.h"
//#include "mainthr.h"

//for test
/*
void shootScreen()
{
    QScreen *screen = QGuiApplication::primaryScreen();

    if (!screen)
        return;

    QPixmap pix = screen->grabWindow(0);
    QByteArray bytes;
    QBuffer buf(&bytes);

    buf.open(QIODevice::ReadWrite);
    pix.save(&buf, "PNG");
    buf.close();

    qDebug() << "Screen size buf is " << buf.size() << ",pos " << buf.pos() << " ,pix s " << pix.size();
    buf.readAll();
    qDebug() << "After reset " << buf.size() << " " << buf.pos();
}
*/
int main(int argc, char *argv[])
{
    //QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    //QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    qRegisterMetaType<MainThr::State>("MainThr::State");
    qmlRegisterType<MainThr>("MainThread",1,0,"MainThreadNamespace");

    MainThr *mainThr = new MainThr();

    SeparateThread *captureThr = new SeparateThread();

    QObject::connect(mainThr, &MainThr::curStateChanged, captureThr, &SeparateThread::curStateChanged);

    captureThr->start(QThread::HighPriority);

    engine.rootContext()->setContextProperty("CaptureWorkThr", captureThr);
    engine.rootContext()->setContextProperty("MainThr", mainThr);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    qDebug() << QCoreApplication::applicationDirPath();

    //shootScreen();
    return app.exec();
}
