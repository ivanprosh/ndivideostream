#include <QDebug>
#include <QScreen>
#include <QBuffer>
#include <QPixmap>
#include <QGuiApplication>

#include "ndisender.h"
#include "capturecontrol.h"
#include "separatethread.h"
#include "command.h"

CaptureControl::CaptureControl(NDISender *_sender, QObject *parent):
    QObject(parent), running(false), count(0), sender(_sender)
{
    //std::function<bool(NDISender&, uint8_t*)> xx = &NDISender::sendFrame;
    if(sender)
        NDIsendCmd = new Command<NDISender>(sender,&NDISender::sendFrame);
}

CaptureControl::~CaptureControl()
{
    if(NDIsendCmd)
        delete NDIsendCmd;
    if(sender)
        delete sender;
}

void CaptureControl::run()
{
    QScreen *screen = QGuiApplication::primaryScreen();

    //QBuffer buf;//(&bytes);
    QImage img;
    //QPixmap pix;

    //qDebug() << "Start work in thr: " << QThread::currentThreadId();

    while(running) {

        //QByteArray bytes;
        //buf.setBuffer(&bytes);

        img = screen->grabWindow(0).toImage();
        //pix = screen->grabWindow(0);
        //if(!img.save(QCoreApplication::applicationDirPath() + "/ttt.png","PNG"))
        //    qDebug() << "Someth wrong with filesave";

        //buf.open(QIODevice::ReadWrite);
        //pix.save(&buf,"BMP");
        //buf.close();
        //QByteArray bytes((char*)img.bits(),img.byteCount());

        if(img.size().width() > 0)
            count++;

//        if(timer.elapsed() - oldCycle > 1000) {
//            qDebug() << " count is " << count-oldCount;
//            //qDebug() << " last buf info: " << img.byteCount() << (int)bytes.at(0) << (int)bytes.at(1) << (int)bytes.at(2) << (int)bytes.at(3);
//            oldCount = count;
//            oldCycle = timer.elapsed();
//        }
        //qDebug() << " count is " << count;

        NDIsendCmd->execute(img.bits());

    }
}
