#include <cstring>
#include <QDebug>

#include "ndisender.h"

NDISender::NDISender(const char *p_connection_string)
{
    if (!NDIlib_initialize())
    {	// Cannot run NDI. Most likely because the CPU is not sufficient (see SDK documentation).
        // you can check this directly with a call to NDIlib_is_supported_CPU()
        throw Error("Cannot run NDI.");
    }

    const NDIlib_send_create_t NDI_send_create_desc = { "My Video", NULL, true, false };

    pNDI_send = NDIlib_send_create(&NDI_send_create_desc);

    if (!pNDI_send)
        throw Error("Empty sender!");

    NDI_connection_type = {
        // The length
        (int)::strlen(p_connection_string),
        // Timecode (synthesized for us !)
        NDIlib_send_timecode_synthesize,
        // The string
        (char*)p_connection_string
    };

    NDIlib_send_add_connection_metadata(pNDI_send, &NDI_connection_type);

    // The resolution to work with
    const int xres = 1920;
    const int yres = 1080;

    NDI_video_frame = {
        // Resolution
        xres, yres,
        // We will stick with RGB color space. Note however that it is generally better to
        // use YCbCr colors spaces if you can since they get end-to-end better video quality
        // and better performance because there is no color dconversion
        NDIlib_FourCC_type_BGRA,
        // The frame-rate
        30000, 1200,
        // The aspect ratio
        (float)xres / (float)yres,
        // This is a progressive frame
        NDIlib_frame_format_type_interleaved,
        // Timecode (synthesized for us !)
        NDIlib_send_timecode_synthesize,
        // The video memory used for this frame
        nullptr, // (uint8_t*)malloc(xres * yres * 4),
        // The line to line stride of this image
        xres * 4
    };
}

bool NDISender::sendFrame(uint8_t *_p_data)
{
    //qDebug() << "NDIsender::send!";

    if(_p_data == nullptr)
        return false;

    NDI_video_frame.p_data = _p_data;
    NDIlib_send_send_video(pNDI_send, &NDI_video_frame);

    return true;
}
