#ifndef SEPARATETHREAD_H
#define SEPARATETHREAD_H

#include <QThread>
#include <QDebug>
#include "mainthr.h"
#include "capturecontrol.h"
//class CaptureControl;

class SeparateThread : public QThread
{
    Q_OBJECT
    Q_PROPERTY(quint32 frCount READ frCount NOTIFY frCountChanged)
    Q_PROPERTY(short fps READ fps NOTIFY fpsChanged)

public:
    SeparateThread(QObject *parent = Q_NULLPTR);
    ~SeparateThread();

    quint32 frCount() const { return m_worker->count;  }
    short fps() const { return m_fps; }

protected:
    void run() override;

signals:
    void error( const QString& text );
    void captureStart();
    void frCountChanged(quint32 frCount);
    void fpsChanged(short fps);

public slots:
    void curStateChanged(MainThr::State);
    void dataUpdate();
    //void setFrCount(quint32 frCount);

private:
    short m_fps;
    quint32 countBefore;
    CaptureControl* m_worker;

    QTimer* _updateTimer;
};

#endif // SEPARATETHREAD_H
