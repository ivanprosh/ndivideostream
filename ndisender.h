#ifndef NDISENDER_H
#define NDISENDER_H

#include "Processing.NDI.Lib.h"
#include <QString>

struct Error
{
    Error(const QString& info):_info(info) {}
    QString _info;
};

class NDISender
{
public:
   NDISender(const char* p_connection_string);
   bool sendFrame(uint8_t* p_data);

private:
   NDIlib_send_instance_t pNDI_send;
   NDIlib_metadata_frame_t NDI_connection_type;
   NDIlib_video_frame_t NDI_video_frame;
};

#endif // NDISENDER_H
